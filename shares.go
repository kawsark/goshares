package main

import (
	"fmt"
	"log"
	"math"
	"strconv"
	"strings"
	"sync"
)

// The Share struct represents a security
type Share struct {
	Name               string
	CompanyName        string `json:"companyName"`
	StatsMap           map[string]interface{}
	Price              float64
	BuyRecommendations int
	//BuyRecMap          map[string]bool
	Analyst         analyst
	Week52HighDelta float64
	Week52LowDelta  float64
}

// Process each Buy String and store result in an array
func (s *Share) processBuyString(buyRecName string, buyrecStr string, wg *sync.WaitGroup, tempMap map[string]bool) {
	//Signal end of buy string processing
	defer wg.Done()

	//Split the buy recommendation string by comma and process each segment
	buyStrings := strings.Split(buyrecStr, ",")
	if Debug {
		fmt.Println(fmt.Sprintf("DEBUG: Security: %s, Buy recs: %s:%s", s.Name, buyRecName, buyrecStr))
	}

	//Parse each segment in this buy strategy
	for _, buySegment := range buyStrings {
		fi := strings.IndexAny(buySegment, "<=>")
		li := strings.LastIndexAny(buySegment, "<=>")
		if fi == -1 {
			log.Fatal(fmt.Sprintf("FATAL: Unable to parse segment in buy recommendation string: %s, segment: %s", buyrecStr, buySegment))
		} else {
			//Get comparison operator
			comparison := buySegment[fi : li+1]
			if Debug {
				fmt.Println(fmt.Sprintf("DEBUG: comparison operator -> %s", comparison))
			}

			//Get left and right pieces
			pieces := strings.Split(buySegment, comparison)
			if Debug {
				fmt.Println(fmt.Sprintf("DEBUG: pieces -> %s", pieces))
			}

			var leftValue, rightValue float64
			var err error

			//Get the left Value
			ls := strings.TrimSpace(pieces[0])
			if ls == "price" {
				// Setting statsmap in goroutine will provide cocurrent map access error
				// Hence looking up value directly instead of map
				leftValue = s.Price
			} else {
				if lv, exists := s.StatsMap[ls]; exists {
					if lv != nil {
						leftValue = lv.(float64)
					} else { // print warning and skip check
						log.Println(fmt.Sprintf("WARNING: Could not process left key and value: %s, %s, for security: %s", ls, lv, s.Name))
						tempMap[buyRecName] = false
						break
					}
				} else {
					if leftValue, err = strconv.ParseFloat(ls, 64); err != nil {
						log.Fatal(fmt.Sprintf("FATAL: Could not convert left string to float: %s", ls))
					}
				}
			}

			//Get the right Value
			rs := strings.TrimSpace(pieces[1])
			if rs == "price" {
				// Setting statsmap in goroutine will provide cocurrent map access error
				// Hence looking up value directly instead of map
				rightValue = s.Price
			} else {
				if rv, exists := s.StatsMap[rs]; exists {
					if rv != nil {
						rightValue = rv.(float64)
					} else {
						log.Println(fmt.Sprintf("WARNING: Could not process right key and value: %s, %s, for security: %s", rs, rv, s.Name))
						tempMap[buyRecName] = false
						break
					}
				} else {
					if rightValue, err = strconv.ParseFloat(rs, 64); err != nil {
						log.Fatal(fmt.Sprintf("FATAL: Could not convert right string to float: %s", rs))
					}
				}
			}

			var buy bool = false

			switch comparison {
			case "=":
				buy = (leftValue == rightValue)

			case "<":
				buy = (leftValue < rightValue)

			case ">":
				buy = (leftValue > rightValue)

			case ">=":
				buy = (leftValue >= rightValue)

			case "<=":
				buy = (leftValue <= rightValue)

			default:
				log.Fatal(fmt.Sprintf("FATAL: Unable to parse segment in buy recommendation string: %s, segment: %s", buyrecStr, buySegment))
			}

			tempMap[buyRecName] = buy
			if !buy {
				break
			}
		}
	}
}

// adds analyst recommendations to the stats map
func (s *Share) setAnalystTargets(a analyst) {
	s.Analyst = a
	s.StatsMap["priceTargetAverage"] = a.PriceTargetAverage
	s.StatsMap["priceTargetHigh"] = a.PriceTargetHigh
	s.StatsMap["priceTargetLow"] = a.PriceTargetLow
	s.StatsMap["numberOfAnalysts"] = a.NumberOfAnalysts
}

// Returns the delta between current price and indicated Float
// TODO: Better error handling
func (s *Share) getPriceFloatDelta(statskey string) float64 {
	var stat float64 = s.StatsMap[statskey].(float64)
	deltaPercent := (s.Price - stat) / s.Price * 100
	return deltaPercent
}

// Returns the delta between current price and 52 weeks high
func (s *Share) get52WeekUpside() float64 {
	x := s.StatsMap["week52high"]
	var share52WeeksHigh float64 = x.(float64)
	deltaPercent := (math.Abs(share52WeeksHigh-s.Price) / s.Price) * 100
	s.Week52HighDelta = deltaPercent
	return deltaPercent
}

// Returns the delta between current price and 52 weeks low
func (s *Share) get52WeekDownside() float64 {
	x := s.StatsMap["week52low"]
	var share52WeeksLow float64 = x.(float64)
	deltaPercent := (math.Abs(share52WeeksLow-s.Price) / s.Price) * 100
	s.Week52LowDelta = deltaPercent
	return deltaPercent
}

// Checks whether the current price point is a threshold % below 52 weeks delta
func (s *Share) check52WeekHighDelta(deltaPercent float64) bool {
	return (s.get52WeekUpside() >= deltaPercent)
}

// Checks whether the current price point is a threshold % below 52 weeks delta
// Returns True if downside is within the delta percent
func (s *Share) check52WeekLowDelta(deltaPercent float64) bool {
	return (deltaPercent >= s.get52WeekDownside())
}

// day5ChangePercent < 0, month1ChangePercent > 0, price > month3ChangePercent, price <= day50MovingAvg, year1ChangePercent < 0
func (s *Share) processBuyRecommendations(screenerGroup *ScreenerGroup) map[string]bool {
	//First update the latest price
	//price := getPrice(s.Name)
	//s.setPrice(price)

	//Initialize the buy recommendations map
	tempMap := make(map[string]bool, len(screenerGroup.Screeners))
	s.BuyRecommendations = 0

	//Parse each string in the Buy strategy map
	if s.check52WeekHighDelta(delta52WeeksMin) {
		var wg sync.WaitGroup
		for _, screener := range screenerGroup.Screeners {
			tempMap[screener.Name] = false
			wg.Add(1)
			go s.processBuyString(screener.Name, screener.Conditions, &wg, tempMap)
			wg.Wait()
			//Increment buy recommendations count
			if tempMap[screener.Name] {
				s.BuyRecommendations++
			}
		}
	} else {
		if Debug {
			fmt.Println(fmt.Sprintf("DEBUG: Excluding security: %s from Buy due to 52 week delta threshold: %.2f%%", s.Name, delta52WeeksMin))
		}
	}

	return tempMap
}

func (s *Share) setStatsMap(statsMap map[string]interface{}) {
	s.StatsMap = statsMap
}

func (s *Share) setStatsFloat(key string, value float64) {
	s.StatsMap[key] = value
}

// Concurrent map write error if part of
func (s *Share) setPrice(p float64) {
	s.Price = p
}

func (s *Share) saveShare(backend Backend) bool {
	return backend.SaveShare(s)
}

func makeShare(name string, readBackend Backend) *Share {
	s := readBackend.LoadShare(name)

	price := getPrice(name)
	s.setPrice(price)

	return s
}
