package main

import (
	"fmt"
	"log"
	"strings"
	"sync"

	"github.com/julienroland/usg"
)

// Category represents a group of securities
type Category struct {
	Name            string
	securityNames   []string
	NumOfSecurities int
	Securities      map[string]*Share
	Sec52HighThr    int
	Sec52LowThr     int
	Week52HighAvg   float64
}

// CategoryRecommendation ... This will be the output from the buy recommendations API
type CategoryRecommendation struct {
	Name              string
	Securities        []string
	NumOfSecurities   int
	NumOfMatches      int
	Buys              []*SecurityRecommendation
	Matches           []string
	Week52Ratio       float64
	Week52HighPercent float64
	Week52LowPercent  float64
	Week52HighAvg     float64
}

// SecurityRecommendation ... This will be populated for each Security that matches one or more buy conditions
type SecurityRecommendation struct {
	Name                    string
	MatchName               []string
	Upside52Week            float64
	Downside52Week          float64
	RSI14                   float64
	PriceDelta9SMAPercent   float64
	PriceDelta180SMAPercent float64
	AnalystTargetPercent    float64
}

func makeCategoryFromString(name string, securitiesStr string, readBackend *Backend) Category {
	var symbols []string = strings.Split(securitiesStr, ",")
	return makeCategory(name, symbols, readBackend)
}

func makeCategory(name string, securityNames []string, readBackend *Backend) Category {
	log.Println(fmt.Sprintf("INFO: Loading Category: %s", name))
	var c Category
	c.Name = name
	c.securityNames = securityNames
	c.Securities = make(map[string]*Share)
	for _, symbol := range securityNames {
		if len(symbol) > 0 {
			s := makeShare(strings.TrimSpace(symbol), *readBackend)
			if s.StatsMap != nil && len(s.CompanyName) > 0 {
				c.Securities[symbol] = s
			} else {
				log.Println(fmt.Sprintf("WARN: Excluding share %s from category %s due to missing stats or invalid company name", symbol, name))
			}
		}
	}
	c.NumOfSecurities = len(c.Securities)

	return c
}

// Saves all shares in this Category
func (c *Category) saveCategoryShares(writeBackend Backend, wg *sync.WaitGroup) int {

	//Signal end of buy string processing
	defer wg.Done()

	var x, y int
	for _, security := range c.Securities {
		if writeBackend.SaveShare(security) {
			x++
		} else {
			y++
		}
	}

	log.Println(fmt.Sprintf("INFO: Saved %d securities in Category: %s", x, c.Name))

	if y > 0 {
		log.Println(fmt.Sprintf("WARN: Unable to save %d securities in Category: %s", y, c.Name))
	}

	return x
}

// Returns an approximate category strenth float based on the number of security close to 52 weeks high and low
func (c *Category) get52WeeksStrength() float64 {
	return (float64(c.Sec52HighThr) - float64(c.Sec52LowThr)) / float64(c.NumOfSecurities) * 100
}

func (c *Category) getBuyRecommendations(screenerGroup *ScreenerGroup) *CategoryRecommendation {
	if Debug {
		log.Println(fmt.Sprintf("%DEBUG: Processing Category: %s", Colors.Reset, c.Name))
	}

	result := CategoryRecommendation{}

	// Populate the Name and number of securities
	result.Name = c.Name
	result.NumOfSecurities = c.NumOfSecurities
	result.Securities = make([]string, 0)
	result.Matches = make([]string, 0)
	result.Buys = make([]*SecurityRecommendation, 0)

	//Start a counter for calculating the average distance to 52 weeks high
	var week52highAverageDeltaSum float64 = 0

	for name, security := range c.Securities {
		tempBuyRecMap := security.processBuyRecommendations(screenerGroup)

		// Update counters for this category
		if security.check52WeekHighDelta(delta52WeeksThrHigh) {
			c.Sec52HighThr = c.Sec52HighThr + 1
		}

		if security.check52WeekLowDelta(delta52WeeksThrLow) {
			c.Sec52LowThr = c.Sec52LowThr + 1
		}

		result.Securities = append(result.Securities, name)

		week52highAverageDeltaSum = week52highAverageDeltaSum + security.Week52HighDelta

		// If this security is a match add it to the Buys array
		if security.BuyRecommendations > 0 {
			sr := SecurityRecommendation{}
			sr.Name = name
			sr.RSI14 = security.StatsMap["rsi"].(float64)
			sr.AnalystTargetPercent = security.getPriceFloatDelta("priceTargetAverage")
			sr.PriceDelta180SMAPercent = security.getPriceFloatDelta("day180sma")
			sr.PriceDelta9SMAPercent = security.getPriceFloatDelta("day9sma")
			sr.Upside52Week = security.get52WeekUpside()
			sr.Downside52Week = security.get52WeekDownside()
			sr.MatchName = make([]string, 0)
			for buyRecName, value := range tempBuyRecMap {
				if value {
					sr.MatchName = append(sr.MatchName, buyRecName)
				}
			}
			result.Buys = append(result.Buys, &sr)
			result.Matches = append(result.Matches, name)
		}

		result.NumOfMatches = len(result.Matches)
	}

	// Update counters
	result.Week52HighPercent = float64(c.Sec52HighThr) / float64(c.NumOfSecurities) * 100
	result.Week52LowPercent = float64(c.Sec52LowThr) / float64(c.NumOfSecurities) * 100
	result.Week52Ratio = c.get52WeeksStrength()
	c.Week52HighAvg = 100.0 - (week52highAverageDeltaSum / float64(c.NumOfSecurities))
	result.Week52HighAvg = c.Week52HighAvg
	if Debug {
		log.Println(fmt.Sprintf("DEBUG: Category [0] %s, Week52HighAvg: %.2f", c.Name, c.Week52HighAvg))
	}

	return &result
}

// Buy recommendation Rendering functions
// Returns an approximate category strenth string based on the number of security close to 52 weeks high and low
func (c *Category) get52WeeksStrengthStr(categoryRecommendation *CategoryRecommendation) string {

	cl := Colors.Green
	if categoryRecommendation.Week52Ratio <= 50 {
		if categoryRecommendation.Week52Ratio > 25 {
			cl = Colors.Orange
		} else {
			cl = Colors.Red
		}
	}
	var result string = fmt.Sprintf("- Avg strength: [ Price %% of 52 weeks high %s %s: %.2f%% %s | Highs %.2f%% | Lows %.2f%% : %sRatio %.2f%% %s ]", Colors.Bold, c.Name, c.Week52HighAvg, Colors.Reset, categoryRecommendation.Week52HighPercent, categoryRecommendation.Week52LowPercent, cl, categoryRecommendation.Week52Ratio, Colors.Green)
	return result
}

func (c *Category) renderCategoryBuyRecommendationsStr(categoryRecommendation *CategoryRecommendation) string {
	var result string = fmt.Sprintf("%s%s%v", Colors.Green, "-- Buys: ", categoryRecommendation.Matches)
	var result2 string = ""
	//Compile all buy recommendations in a single array
	for _, sr := range categoryRecommendation.Buys {
		var deltaPercent float64

		// Process the 9 day SMA
		/* deltaPercent = sr.PriceDelta9SMAPercent
		var day9SMADeltaStr string
		if deltaPercent < 0 {
			day9SMADeltaStr = fmt.Sprintf("%s%.2f%%%s", Colors.Red, deltaPercent, Colors.Green)
		} else {
			day9SMADeltaStr = fmt.Sprintf("%.2f%%", deltaPercent)
		} */

		// Process the 180 day SMA
		/* deltaPercent = sr.PriceDelta180SMAPercent
		var day180SMADeltaStr string
		if deltaPercent < 0 {
			day180SMADeltaStr = fmt.Sprintf("%s%.2f%%%s", Colors.Red, deltaPercent, Colors.Green)
		} else {
			day180SMADeltaStr = fmt.Sprintf("%.2f%%", deltaPercent)
		} */

		// Process the analyst price target
		deltaPercent = sr.AnalystTargetPercent
		var analystTargetDeltaStr string
		if deltaPercent > 0 {
			analystTargetDeltaStr = fmt.Sprintf("%s%.2f%%%s", Colors.Red, deltaPercent, Colors.Green)
		} else {
			analystTargetDeltaStr = fmt.Sprintf("%.2f%%", deltaPercent)
		}

		x_factor := (sr.Upside52Week - deltaPercent) + (50.0 - sr.RSI14)

		//result2 = result2 + fmt.Sprintf("-- [%s %s%.2f%% %s%s%.2f%%%s RSI: %.2f] (", name, usg.Get.ArrowUp, get52WeekUpside(&security), Red, usg.Get.ArrowDown, get52WeekDownside(&security), Green, rsi)
		result2 = result2 + fmt.Sprintf("-- [%s %s%.2f%% %s%s%.2f%%%s RSI: %.2f Analyst: %s %sX: %.2f%s%s %v]\n", sr.Name, usg.Get.ArrowUp, sr.Upside52Week, Colors.Red, usg.Get.ArrowDown, sr.Downside52Week, Colors.Green, sr.RSI14, analystTargetDeltaStr, Colors.Bold, x_factor, Colors.Reset, Colors.Green, sr.MatchName)
	}
	buys := result + "\n" + result2 + Colors.Reset
	strength := c.get52WeeksStrengthStr(categoryRecommendation)
	finalResult := fmt.Sprintf("\n%s- %s%s (%d)%s%s: %v\n%s%s", Colors.Reset, Colors.Bold, categoryRecommendation.Name, categoryRecommendation.NumOfSecurities, Colors.Reset, Colors.Green, categoryRecommendation.Securities, buys, strength)

	return finalResult
}
