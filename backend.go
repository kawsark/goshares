package main

// Backend ... This interface allows for reading or writing a Share
type Backend interface {
	SaveShare(s *Share) bool
	LoadShare(symbol string) *Share
	LoadScreenerGroup(id string) (*ScreenerGroup, error)
}
