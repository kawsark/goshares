//https://stackoverflow.com/questions/4197591/parsing-url-hash-fragment-identifier-with-javascript/57018898#57018898 
const parsedHash = new URLSearchParams(
  window.location.hash.substring(1) // skip the first char (#)
);

//Check for Access Token
var token = parsedHash.get("id_token");

//Check for ID Token
if (token == null) {
    token = parsedHash.get("access_token");
}

    if (token == null) {
	    console.log("access_token or id_token not found")
    } else{
        console.log(token)
	    document.getElementById("token").innerHTML = token;

	    var decoded = jwt_decode(token);
        console.log(decoded)
	    document.getElementById("decoded").innerHTML = JSON.stringify(decoded, null, 4);
    }
