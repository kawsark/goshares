package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
)

// FileBackend ... This backend uses a local file system as the persistence layer
type FileBackend struct {
	DataPrefix string
}

// SaveShare ... Saves a Share to a flat file
func (f *FileBackend) SaveShare(s *Share) bool {
	// Marshall the Share struct to json
	file, err := json.Marshal(s)
	if err != nil {
		log.Println(fmt.Sprintf("ERROR: Could not marshal json for: %s, err: %s", s.Name, err))
		return false
	}

	// Persist to file
	filepath := f.DataPrefix + s.Name + "-stats.json"
	err = ioutil.WriteFile(filepath, file, 0644)
	if err != nil {
		log.Println(fmt.Sprintf("ERROR: Could save Share to json file. Symbol Name: %s, err: %s", s.Name, err))
		return false
	}

	return true
}

// LoadShare ... Loads a Share from a flat file
func (f *FileBackend) LoadShare(symbol string) *Share {
	filepath := f.DataPrefix + symbol + "-stats.json"
	file, err := ioutil.ReadFile(filepath)

	if err != nil {
		log.Println(fmt.Sprintf("ERROR: Could not read from json file: %s, err: %s", filepath, err))
	}

	// Initialize a share struct
	s := Share{}
	err = json.Unmarshal([]byte(file), &s)
	if err != nil {
		log.Println(fmt.Sprintf("ERROR: Could not unmarshal json for: %s, filename: %s, err: %s", symbol, filepath, err))
		s.Name = symbol
	}

	if Debug {
		log.Println(fmt.Sprintf("DEBUG: FileBackend: Loaded share: %s, struct: %v", symbol, s))
	}

	return &s
}

// LoadScreenerGroup ... Load a screener group from flat file and return a buyRecommendations Map
func (f *FileBackend) LoadScreenerGroup(id string) (*ScreenerGroup, error) {
	filepath := f.DataPrefix + "SG-" + id + ".json"

	file, err := ioutil.ReadFile(filepath)
	if err != nil {
		log.Println(fmt.Sprintf("ERROR: Could read from json file: %s, err: %s", filepath, err))
		return nil, errors.New("Invalid Screener Group name")
	}

	// Initialize a share struct
	sg := ScreenerGroup{}
	err = json.Unmarshal([]byte(file), &sg)

	if err != nil {
		log.Println(fmt.Sprintf("ERROR: Could not unmarshal Screener Group json for: %s, filename: %s, err: %s", id, filepath, err))
	}

	if Debug {
		log.Println(fmt.Sprintf("DEBUG: FileBackend: Loaded Secreener Group ID: %s, struct: %v", id, sg))
	}

	return &sg, nil

}
