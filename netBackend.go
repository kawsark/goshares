package main

import (
	"errors"
	"fmt"
	"log"
)

// NetBackend ... Uses the IEX API to instantiate Securities
type NetBackend struct {
}

// SaveShare ... Not implemented in this backend
func (n *NetBackend) SaveShare(s *Share) bool {
	log.Println("ERROR: The Network Backend is read only")
	return false
}

// LoadScreenerGroup ... Not implemented in this backend
func (n *NetBackend) LoadScreenerGroup(id string) (*ScreenerGroup, error) {
	log.Println("ERROR: The Network Backend does not suppprt Screeners")
	return nil, errors.New("Not implemented")
}

// LoadShare ... loads Share from API
func (n *NetBackend) LoadShare(symbol string) *Share {
	var s Share
	s.Name = symbol

	// Set the key stats map
	statsMap := getStats(symbol)
	s.setStatsMap(statsMap)

	cn := statsMap["companyName"]
	if cn != nil {
		s.CompanyName = cn.(string)
	} else {
		s.CompanyName = ""
	}

	// Set technical indicators
	s.setStatsFloat("rsi", getRSI(symbol, 14))
	s.setStatsFloat("day180sma", getSMA(symbol, 180))
	s.setStatsFloat("day9sma", getSMA(symbol, 9))

	// Set Analyst targets
	s.setAnalystTargets(getAnalystTargets(symbol))

	if Debug {
		log.Println(fmt.Sprintf("DEBUG: NetBackend: Loaded share: %s, struct: %v", symbol, s))
	}

	return &s
}
