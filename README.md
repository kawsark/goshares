## Using Goshares

### CLI

```bash
export IEX_KEY=<api_key>

# When running for the first time, use the -get option to download information on Securities
# Note: this takes a long time to laod
goshares -get

# When running subsequently
goshares
```


## Goshares Architeture

The Goshares application uses these concepts:
- Group: A Group represents a high level grouping of securities. For example, it could represent an index such as S&P500, Someones trading account, or a strategy such as "High Yield"
- Category: A Category belongs to a Group. For example, it could represent a category within S&P500 or a person's portfolio or watchlist


## TODO
- See issues.md