variable "account_id" {
  
}

variable "vpc_cidr_range" {
  description = "The CIDR Range of the EKS VPC"
}

variable "vpc_id" {
  description = "The VPC with which to establish a vpc link"
}

variable "private_subnet_ids" {
  description = "The efs subnets where EKS nodes have been deployed"
}

variable "public_subnet_ids" {
  description = "The subnets where bastion server has been deployed"
}

variable "repository_name" {
  default = "goshares"
}

variable "repository_description" {
  default = "AWS code commit repository for the goshares application"
}

variable "default_branch" {
  default = "master"
}

variable "tags" {
  default = {
    "app" : "goshares"
  }
}

variable "am_default_branch" {
  default = "main"
}

variable "am_tags" {
  default = {
    "app" : "anymarket"
  }
}