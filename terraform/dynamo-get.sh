#!/bin/bash

usage="dynamo-put.sh <dynamodbtablename> <Symbol>"

cat << EOF >key.json
  {
    "Symbol": {"S": "$2"}
  }
EOF

item=$(aws dynamodb get-item \
    --region us-east-2 \
    --table-name $1 \
    --key file://key.json)

strategy=$(echo $item | jq .Item.Strategy.S)
symbol=$(echo $item | jq .Item.Symbol.S)
position=$(echo $item | jq .Item.Position.S)
ratio=$(echo $item | jq .Item.PositionRatioPercent.S)

echo $item | jq .

  
