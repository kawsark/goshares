#!/bin/bash

usage="dynamo-put.sh <dynamodbtablename> <Symbol> <Position> <PositionRatio> <Strategy>"

table_name=$2

cat << EOF >item.json
  {
    "Symbol": {"S": "$2"},
    "Position": {"N": "$3"},
    "PositionRatioPercent": {"S": "$4"},
    "Strategy": {"S": "$5"}
  }
EOF

aws dynamodb put-item \
    --region us-east-2 \
    --table-name $1 \
    --item file://item.json
