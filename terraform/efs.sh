#!/bin/bash
# This script uses the EFS Mount helper tool to mount a filesystem and uses TLS
# https://docs.aws.amazon.com/efs/latest/ug/mounting-fs-mount-helper-ec2-linux.html
if [[ -z $1 ]]; then
  echo "INFO: Looking up EFS ID from terraform"
  export id=$(terraform output -raw efs_id)
else
  export id=$1
fi

echo "INFO: Using EFS ID: ${id}"

target=$HOME/efs
echo "INFO: Using mount target: ${target}"
mkdir -p ${target}

#Note: the EFS Helper utility should already be installed. See EC2 Userdata script

# Mount EFS data directory and copy goshares files
#sudo mount -t nfs -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2,noresvport ${id}:/ ${target}
sudo mount -t efs -o tls ${id} ${target}
sudo chown ubuntu:ubuntu ${target}

cd ../
source env.sh
cp data/strategy.json ${target}/
cp data/SG*.json ${target}/
./goshares -get

