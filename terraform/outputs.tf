output "irsa_policy_arn" {
  value = aws_iam_policy.eks-irsa-policy.arn
}

output "efs_id" {
  value = aws_efs_file_system.efs.id
}

output "efs_dns" {
  value = aws_efs_file_system.efs.dns_name
}

output "dynamodb_name" {
  value = aws_dynamodb_table.anymarket-dynamodb-table.name
}

output "dynamodb_name_customer" {
  value = aws_dynamodb_table.anymarket-dynamodb-table-customer1.name
}