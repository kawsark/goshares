resource "aws_efs_file_system" "efs" {
  creation_token = "goshares-eks"

  performance_mode = "generalPurpose"
  throughput_mode  = "bursting"
  encrypted        = true

  # lifecycle_policy {
  #   transition_to_ia = "AFTER_30_DAYS"
  # }

  tags = {
    Name = "eks"
    App = "goshares"
  }
}

# Mount target for public subnet (Bastion)
/* resource "aws_efs_mount_target" "bastion-zone-a" {
  file_system_id  = aws_efs_file_system.efs.id
  subnet_id       = var.public_subnet_ids[0]
  security_groups = [aws_security_group.MyEfsSecurityGroup.id]
} */

# resource "aws_efs_mount_target" "bastion-zone-b" {
#   file_system_id  = aws_efs_file_system.efs.id
#   subnet_id       = var.public_subnet_ids[1]
#   security_groups = [aws_security_group.MyEfsSecurityGroup.id]
# }

# Mount target for private subnet
resource "aws_efs_mount_target" "zone-a" {
  file_system_id  = aws_efs_file_system.efs.id
  subnet_id       = var.private_subnet_ids[0]
  security_groups = [aws_security_group.MyEfsSecurityGroup.id]
}

# Mount target for private subnet
resource "aws_efs_mount_target" "zone-b" {
  file_system_id  = aws_efs_file_system.efs.id
  subnet_id       = var.private_subnet_ids[1]
  security_groups = [aws_security_group.MyEfsSecurityGroup.id]
}

resource "aws_security_group" "MyEfsSecurityGroup" {
  name   = "MyEfsSecurityGroup"
  description = "My EFS security group"
  vpc_id = var.vpc_id

  ingress {
    from_port   = 2049
    to_port     = 2049
    protocol    = "TCP"
    cidr_blocks = var.vpc_cidr_range
  }
}

# Mount filesystem using local_exec
resource "null_resource" "efs_mount" {
  provisioner "local-exec" {
    command = "./efs.sh ${aws_efs_file_system.efs.id}"
  }
}
