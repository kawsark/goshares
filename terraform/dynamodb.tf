resource "aws_dynamodb_table" "anymarket-dynamodb-table" {
  name           = "Anymarket"
  billing_mode   = "PROVISIONED"
  read_capacity  = 5
  write_capacity = 5
  hash_key       = "Symbol"
  range_key      = "Price"

  attribute {
    name = "Symbol"
    type = "S"
  }

  attribute {
    name = "Price"
    type = "N"
  }


  tags = {
    Tenant     = "Shared"
    App        = "Anymarket"
    Environment = "Dev"
  }
}

resource "aws_dynamodb_table" "anymarket-dynamodb-table-customer1" {
  name           = "Anymarket-customer1"
  billing_mode   = "PROVISIONED"
  read_capacity  = 5
  write_capacity = 5
  hash_key       = "Symbol"

  attribute {
    name = "Symbol"
    type = "S"
  }

    tags = {
    Tenant     = "Customer 1"
    App        = "Anymarket"
    Environment = "Dev"
  }
}