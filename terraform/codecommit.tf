resource "aws_codecommit_repository" "repo" {
  repository_name = var.repository_name
  description     = var.repository_description
  default_branch  = var.default_branch

  # Tags
  tags = var.tags

}

resource "aws_codecommit_repository" "repo-app" {
  repository_name = "anymarket-app"
  description     = "Repository for anymarket application services"
  default_branch  = var.am_default_branch
  tags = var.am_tags

}

resource "aws_codecommit_repository" "repo-iac" {
  repository_name = "anymarket-iac"
  description     = "Anymarket application environment"
  default_branch  = var.am_default_branch
  tags = var.am_tags

}