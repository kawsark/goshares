resource "aws_iam_policy" "eks-irsa-policy" {
  name        = "eksctl-irsa-policy"
  path        = "/"
  description = "Policy for EKS IRSA"

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  policy = jsonencode({
    "Version":"2012-10-17",
    "Statement":[
        {
            "Effect":"Allow",
            "Action": "s3:ListAllMyBuckets",
            "Resource":"*"
        },
        {
            "Sid": "Dynamo",
            "Effect": "Allow",
            "Action": "dynamodb:*",
            "Resource": "arn:aws:dynamodb:us-east-2:${var.account_id}:table/Anymarket-customer1"
        },
        {
          "Sid":"allowputmetric",
          "Effect": "Allow",
          "Action": [
            "cloudwatch:GetMetricStatistics",
            "cloudwatch:ListMetrics",
            "cloudwatch:PutMetricData",
          ],
          "Resource": "*"
      }
    ]
  })
}
