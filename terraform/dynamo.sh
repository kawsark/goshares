#!/bin/bash

usage="dynamo.sh <filename> <dynamodbtablename>"
[[ -z $1 ]] && echo $usage && exit 1
[[ -z $2 ]] && echo $usage && exit 1

file=$1
companies=$(cat ${file} | jq -r .companies[])
table_name=$2

for s in ${companies}
do
  echo "INFO: Symbol: $s"
  price=$(curl -s https://cloud.iexapis.com/stable/stock/${s}/price?token=${IEX_KEY})
  stats=$(curl -s https://cloud.iexapis.com/stable/stock/${s}/company?token=${IEX_KEY})
  companyName=$(echo ${stats} | jq -r .companyName)
  exchange=$(echo ${stats} | jq -r .exchange)
  industry=$(echo ${stats} | jq -r .industry)
  employees=$(echo ${stats} | jq .employees)

cat << EOF >item.json
  {
    "Symbol": {"S": "$s"},
    "Price": {"N": "$price"},
    "companyName": {"S": "$companyName"},
    "exchange": {"S": "$exchange"},
    "employees": {"N": "$employees"},
    "industry": {"S": "$industry"}
  }
EOF

aws dynamodb put-item \
    --region us-east-2 \
    --table-name $table_name \
    --item file://item.json

done