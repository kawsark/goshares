package main

import (
	"encoding/json"
	"fmt"
	"gitlab.com/kawsark/goshares/identity"
	"log"
	"net/http"
	"os"
	"strconv"
	"github.com/aws/aws-sdk-go/aws"
    "github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/cloudwatch"
	"strings"
	"time"
)

// HTTPServer ... an instance of the server to be initialized in main()
var HTTPServer *Server
var serverDebug bool = false

// Server ... The Goshares server
type Server struct {
	Name            string
	ReadBackend     Backend
	ScreenerBackend Backend
	APIGroup        *Group
	Tenant          *identity.Tenant
	AWSSession		*session.Session
	AWSCloudWatch	bool
}

type categoryInput struct {
	Categoryname string   `json:"categoryname"`
	Securities   []string `json:"securities"`
	Strategyset  string   `json:"strategyset"`
}

func checkAndLogIdentity(req *http.Request) (string, string, string) {
	var user, email, tenant string

	//Check for Authorization Header
	if val, ok := req.Header["Authorization"]; ok {
		if serverDebug{
			log.Println("DEBUG: Found Authorization Header: ", val)
		}
		i := identity.Identity{}
		user, email = i.GetClientName(val[0])
	}else{
		if serverDebug {
			log.Println("DEBUG: req:", req)
		}
	}

	if len(email) > 0 {
		parts := strings.Split(email,"@")
		tenant = parts[1]
	}else{
		tenant = "guest"
	}

	if serverDebug {
		log.Println("DEBUG: user:", user)
		log.Println("DEBUG: email:", email)
		log.Println("DEBUG: tenant:", tenant)
	}

	return user, email, tenant
}

func homePage(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	_, _, tenant := checkAndLogIdentity(req)

	switch req.Method {
	case "GET":
		{
			fmt.Fprintf(w, "{\"message\":\"evaluate-get-endpoint\"}")
			log.Println("INFO: Endpoint Hit: homePage (GET)")
			logMetric(tenant,"evaluate","200 OK")
			w.WriteHeader(http.StatusOK)
		}
	case "POST":
		{
			decoder := json.NewDecoder(req.Body)
			input := categoryInput{}
			err := decoder.Decode(&input)
			if err == nil {

				if serverDebug {
					log.Println(fmt.Printf("DEBUG: CategoryInput: %v", input))
				}

				var target_category Category

				if len(input.Categoryname) > 0 {
					group_categories := &HTTPServer.APIGroup.Categories
					category, ok := (*group_categories)[input.Categoryname]
					if ok {
						target_category = *category
					} else {
						http.Error(w, "{\"ERROR\":\"Category not found\"}", http.StatusNotFound)
						fmt.Println("ERROR: Category name not found: %s", input.Categoryname)
						logMetric(tenant,"evaluate","404 NotFound")
						return
					}
				} else { // Use supplied to create a temporary category
					target_category = makeCategory("api-category", input.Securities, &HTTPServer.ReadBackend)
				}

				sg, err := HTTPServer.ScreenerBackend.LoadScreenerGroup(input.Strategyset)
				if err != nil {
					http.Error(w, "{\"ERROR\":\"Strategyset not found\"}", http.StatusNotFound)
					fmt.Println("ERROR: Error loading Strategyset, check name not found: %s, %s", input.Strategyset, err)
					logMetric(tenant,"evaluate","404 NotFound")
					return
				}

				br := target_category.getBuyRecommendations(sg)

				write, err := json.Marshal(br)
				if err != nil {
					http.Error(w, "{\"ERROR\":\"Internal error: JSON Marshall\"}", http.StatusInternalServerError)
					fmt.Println("ERROR: Could not marshal json for category:", input, err)
					logMetric(tenant,"evaluate","500 InternalError")
					return
				}

				w.WriteHeader(http.StatusOK)
				logMetric(tenant,"evaluate","200 OK")
				fmt.Fprintf(w, string(write))

			} else {
				fmt.Println("ERROR: Unable to decode body:", req.Body)
				fmt.Fprintf(w, fmt.Sprintf("ERROR: %s", err))
				http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
				logMetric(tenant,"evaluate","400 BadRequest")
				return
			}
		}
	}
}

// Implement a health check endpoint
// Setting response codes: https://go.dev/src/net/http/status.go
func healthEndpoint(w http.ResponseWriter, req *http.Request) {
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	resp := make(map[string]interface{})
	resp["health"] = "OK"

	group_categories := &HTTPServer.APIGroup.Categories
	resp["cateogires_loaded"] = strconv.Itoa(len((*group_categories)))
	resp["Method"] = req.Method
	resp["URL"] = req.URL
	resp["Proto"] = req.Proto
	resp["Host"] = req.Host
	resp["RemoteAddr"] = req.RemoteAddr

	request_headers := make(map[string][]string)

	resp["request_headers"] = request_headers

	//Iterate over all header fields
	for k, v := range req.Header {
		request_headers[k] = v
	}

	jsonResp, err := json.Marshal(resp)
	if err != nil {
		http.Error(w, "{\"ERROR\":\"Internal error: JSON Marshall\"}", http.StatusInternalServerError)
		fmt.Println("ERROR: Could not marshal json:", err)
	} else {
		w.Write(jsonResp)
	}

	return
}

func handleRequests() {
	http.HandleFunc("/", homePage)
	http.HandleFunc("/health", healthEndpoint)

	log.Fatal(http.ListenAndServe(":32000", nil))
}

func init() {
	// Lookup DEBUG environment variable and set it globally
	v := os.Getenv("SERVER_DEBUG")
	serverDebug, _ = strconv.ParseBool(v)

}

func logMetric(tenant string, service string, result string){

	if HTTPServer.AWSCloudWatch {
			// Create new cloudwatch client.
			svc := cloudwatch.New(HTTPServer.AWSSession)

			_, err := svc.PutMetricData(&cloudwatch.PutMetricDataInput{
				Namespace: aws.String("AnyMarket/customer/"+tenant),
				MetricData: []*cloudwatch.MetricDatum{
					&cloudwatch.MetricDatum{
						MetricName: aws.String("invocation"),
						Unit:       aws.String("Count"),
						Value:      aws.Float64(1),
						Timestamp:	aws.Time(time.Now()),
						Dimensions: []*cloudwatch.Dimension{
							&cloudwatch.Dimension{
								Name:  aws.String("Service"),
								Value: aws.String(service),
							},
							&cloudwatch.Dimension{
								Name:  aws.String("Result"),
								Value: aws.String(result),
							},
						},
					},
				},
			})
			if err != nil {
				log.Println("ERROR: Error adding metrics:", err.Error())
			}else{
				if serverDebug{
					log.Println("DEBUG: Added CW metric")
				}
			}
	}else{
		if serverDebug{
			log.Println(fmt.Sprintf("{ \"invocation\": %d, \"Service\": \"%s\", \"Timestamp: \" : \"%s\"}",1,service,time.Now()))
		}
	}


}
