package: build
	sudo docker build -t kawsark/goshares:0.1 .

build: vendor build_darwin
	go build -o goshares *.go

# End-to-end automation for testing goshares in K8S
# Dependencies, terraform-aws-network-dev provisioned
main: package
	./mainbuilder.sh

# Tear down testing infrastructure
# Dependencies, terraform-aws-network-dev provisioned
destroy:
	./mainbuilder.sh --destroy

vendor:
	go mod vendor

build_darwin:
	env GOOS=darwin GOARCH=amd64 go build -o goshares_d *.go

run_server: build
	./goshares -server

run_binary: build
	./goshares

docker_push: package
	sudo docker image push kawsark/goshares:0.1

ecr_push: package
	./ecr_push.sh

# Note: this target assumes that substituions are already made in deployment-default.yaml
# See mainbuilder.sh for new deploy
k8s_run: package
	cd k8s; kubectl delete -f deployment-default.yaml
	cd k8s; kubectl apply -f deployment-default.yaml
	cd k8s; kubectl apply -f nodeport-default.yaml
	cd k8s; kubectl apply -f goshares-lb.yaml

docker_run: package
	sudo docker rm -f goshares
	sudo docker run -dit --name goshares -p 80:32000 kawsark/goshares:0.1
	curl localhost/health

ui_push:
	cd ui; aws s3 cp index.html s3://anymarketui
	cd ui; aws s3 cp anymarket.js s3://anymarketui
	cd ui; aws s3 cp jwt-decode.js s3://anymarketui
	
	aws cloudfront create-invalidation --distribution-id ${cloudfront_id} --paths "/*" --output text

clean:
	rm .goshares
	rm .goshares_d

.PHONY: vendor
