#!/bin/bash
source env.sh
[[ -z ${ACCOUNT_ID} ]] && echo "Please set account ID" && exit 1

aws ecr get-login-password --region us-east-2 | sudo docker login --username AWS --password-stdin ${ACCOUNT_ID}.dkr.ecr.us-east-2.amazonaws.com
sudo docker tag kawsark/goshares:0.1 ${ACCOUNT_ID}.dkr.ecr.us-east-2.amazonaws.com/goshares:latest
sudo docker push ${ACCOUNT_ID}.dkr.ecr.us-east-2.amazonaws.com/goshares:latest