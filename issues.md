4. Cache api-category. Currently during high concurrency makeShare calls are triggering 429 for IEX

3. Test with reloading price during processBuyRecommendation
With caching, it should not trigger 429
TODO: Use concurrency safe PriceCache Map (Redis), Currently due to high concurrency, PriceCache can only be updated during startup

2. Error when using Alpine image:
```bash
exec /goshares: no such file or directory
```

References:
- https://tutorialedge.net/golang/go-docker-tutorial/#
- https://stackoverflow.com/questions/49079981/golang-app-in-docker-exec-user-process-caused-no-such-file-or-directory 
- https://stackoverflow.com/questions/71262223/why-does-switching-the-base-image-in-a-multi-stage-docker-build-from-distroless/71262352#71262352

1. [RESOLVED] Handle errors when a Security name is invalid
```bash
ERROR: Could not convert price string for SNE, to double value: Unknown symbol strconv.ParseFloat: parsing "Unknown symbol": invalid syntax
panic: interface conversion: interface {} is nil, not float64

goroutine 1 [running]:
main.(*Share).get52WeekUpside(...)
	/home/ubuntu/goshares/shares.go:137
main.(*Share).check52WeekHighDelta(...)
	/home/ubuntu/goshares/shares.go:154
main.(*Share).processBuyRecommendations(0xc00012a100, 0xc0004084b0)
	/home/ubuntu/goshares/shares.go:174 +0x4fd
main.(*Category).getBuyRecommendations(0xc0006b5e28, 0xc0000a6008?)
	/home/ubuntu/goshares/category.go:118 +0x2b7
main.processBuyRecommendations({0xc0000aa340, 0x6, 0x7?}, 0x1e005b8?)
	/home/ubuntu/goshares/main.go:34 +0x3ae
main.main()
	/home/ubuntu/goshares/main.go:160 +0x125
```
