package main

import (
	"errors"
	"fmt"
	"log"
	"github.com/go-redis/redis"
	"os"
)

// CacheBackend ... Uses the IEX API to instantiate Securities
type CacheBackend struct {
	redisClient *redis.Client
}

// init ... Initialize the Redis client
func (n *CacheBackend) init() bool {
	var result bool

	redisaddr, set := os.LookupEnv("REDIS_ADDR")
	if !set {
		redisaddr = "localhost:6379"
	}

	log.Println("INFO: Initializing Redis client: ", redisaddr)

	n.redisClient = redis.NewClient(&redis.Options{
		Addr: redisaddr,
		Password: "",
		DB: 0,
	})

	pong, err := n.redisClient.Ping().Result()
	if err != nil{
		log.Println("WARN: Could not reach Redis using Hostname: ", err)	
	}else{
		result = true
		log.Println("INFO: Successfully initialized Redis:", pong)	
	}

	return result

}

// SaveShare ... Write to Redis
func (n *CacheBackend) SaveShare(s *Share) bool {
	log.Println("ERROR: The Network Backend is read only")
	return false
}

// LoadScreenerGroup ... Not implemented in this backend
func (n *CacheBackend) LoadScreenerGroup(id string) (*ScreenerGroup, error) {
	log.Println("ERROR: The Network Backend does not suppprt Screeners")
	return nil, errors.New("Not implemented")
}

// LoadShare ... loads Share from API
func (n *CacheBackend) LoadShare(symbol string) *Share {
	var s Share
	s.Name = symbol

	// Set the key stats map
	statsMap := getStats(symbol)
	s.setStatsMap(statsMap)

	cn := statsMap["companyName"]
	if cn != nil {
		s.CompanyName = cn.(string)
	} else {
		s.CompanyName = ""
	}

	// Set technical indicators
	s.setStatsFloat("rsi", getRSI(symbol, 14))
	s.setStatsFloat("day180sma", getSMA(symbol, 180))
	s.setStatsFloat("day9sma", getSMA(symbol, 9))

	// Set Analyst targets
	s.setAnalystTargets(getAnalystTargets(symbol))

	if Debug {
		log.Println(fmt.Sprintf("DEBUG: CacheBackend: Loaded share: %s, struct: %v", symbol, s))
	}

	return &s
}
