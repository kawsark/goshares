package main

// ScreenerGroup ... A group of Screeners that can be uniquely identified
type ScreenerGroup struct {
	ID        string     `json:"id"`
	Screeners []Screener `json:"screeners"`
}

// Screener ... A screener has a name and a set of conditions to match
type Screener struct {
	Name        string `json:"name"`
	Description string `json:"description"`
	Conditions  string `json:"conditions"`
}
