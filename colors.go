package main

import "runtime"

// Colors ... Package wide instance to reference color strings
var Colors *colors = &colors{}

type colors struct {
	Reset  string
	Bold   string
	Red    string
	Green  string
	Yellow string
	Orange string
	Blue   string
	Purple string
	Cyan   string
	Gray   string
	White  string
}

func init() {
	if runtime.GOOS != "windows" {
		Colors.Reset = "\033[0m"
		Colors.Red = "\033[31m"
		Colors.Green = "\033[32m"
		Colors.Yellow = "\033[33m"
		Colors.Blue = "\033[34m"
		Colors.Purple = "\033[35m"
		Colors.Cyan = "\033[36m"
		Colors.Gray = "\033[37m"
		Colors.White = "\033[97m"
		Colors.Bold = "\033[1m"
		Colors.Orange = "\033[93m"
	} else {
		// Windows terminal does not support colors
		Colors.Reset = ""
		Colors.Bold = ""
		Colors.Red = ""
		Colors.Green = ""
		Colors.Yellow = ""
		Colors.Blue = ""
		Colors.Purple = ""
		Colors.Cyan = ""
		Colors.Gray = ""
		Colors.White = ""
		Colors.Orange = ""
	}
}
