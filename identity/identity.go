package identity

import "github.com/golang-jwt/jwt/v4"
import "fmt"

type Identity struct {
	Name            string
	Jwt				string
}

func (i *Identity) GetClientName(tokenString string) (string,string){

	var user string
	var email string

	token, _, err := new(jwt.Parser).ParseUnverified(tokenString, jwt.MapClaims{})
    if err == nil {
		if claims, ok := token.Claims.(jwt.MapClaims); ok {
			user = claims["cognito:username"].(string)
			email = claims["email"].(string)
			//for k, v := range claims {
			//	fmt.Println(k, "value is", v)
			//}
		} else {
			fmt.Println("ERROR: claims parse error: ", err)
		}
    }else{
		fmt.Println("ERROR: jwt parse error: ", err)
	}

	return user, email
}