#!/bin/bash

source env.sh

if [[ $1 == "--destroy" ]]; then
    cd eks; ./eksctl.sh --destroy
else

    # Provision EKS Cluster
    echo "INFO: Stage 1/3 - Creating cluster"
    cd eks
    ./eksctl.sh
    
    # Provision applications
    echo "INFO: Stage 2/3 - Creating applications"
    cd ../k8s

    # Provision Dashboard App
    # https://docs.aws.amazon.com/eks/latest/userguide/dashboard-tutorial.html
    kubectl apply -f dashboard.yaml
    kubectl apply -f eks-admin-service-account.yaml
    
    echo "INFO: Dashboad app installed"
    echo "INFO: run kubectl proxy, start SSH Tunnel and login using eks-admin-token.txt"
    token=$(kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep eks-admin | awk '{print $1}') | grep "token:" | awk '{print $NF}')
    echo $token > eks-admin-token.txt


    # Deploy EFS Filesystem
    sed -i -e 's/EFSID/'${EFSID}'/g' anymarket-efs.yaml
    kubectl apply -f anymarket-efs.yaml
    git restore -- anymarket-efs.yaml

    # Deploy AWS
    kubectl apply -f anymarket-ns.yaml
    sed -i -e 's/IEX_KEY_VALUE/'"${IEX_KEY}"'/g' -e 's/ACCOUNTID/'"${ACCOUNT_ID}"'/g' anymarket-deployment.yaml
    kubectl apply -f anymarket-deployment.yaml
    git restore -- anymarket-deployment.yaml

    kubectl apply -f anymarket-ingress.yaml

    #1. Deploy Goshares
    sed -i -e 's/IEX_KEY_VALUE/'"${IEX_KEY}"'/g' -e 's/ACCOUNTID/'"${ACCOUNT_ID}"'/g' goshares-deployment.yaml
    git restore -- goshares-deployment.yaml
    kubectl apply -f goshares-nodeport.yaml
    #kubectl apply -f goshares-lb.yaml

    #1. Deploy the NS and ServiceAccount
    sed -i -e 's/ACCOUNTID/'"${ACCOUNT_ID}"'/g' anymarket-na-sa.yaml
    kubectl apply -f anymarket-na-sa.yaml
    git restore -- anymarket-deployment.yaml

    #1. Create the Deployment object
    sed -i -e 's/IEX_KEY_VALUE/'"${IEX_KEY}"'/g' -e 's/ACCOUNTID/'"${ACCOUNT_ID}"'/g' anymarket-deployment.yaml
    kubectl apply -f goshares-deployment.yaml
    git restore -- goshares-deployment.yaml

    #1. Create Network components
    kubectl apply -f anycompany-ingress.yaml
    
    #1. Deploy aws-cli for Dynamodb and EFS tests
    sed -i -e 's/IEX_KEY_VALUE/'"${IEX_KEY}"'/g' anymarket-cli.yaml
    kubectl apply -f anymarket-cli.yaml
    git restore -- anymarket-cli.yaml
    #exec yum install jq -y
    #wget get.sh
    #wget push.sh
    echo "INFO: **Complete CLI Setup**"

    # IRSA Test
    #sed -i -e 's/ACCOUNTID/'"${ACCOUNT_ID}"'/g' s3.yaml
    #kubectl apply -f s3.yaml
    #git restore -- s3.yaml
    #echo "INFO: Test IRSA using command below"
    #echo "kubectl exec -it aws-cli -n staging -- aws s3api list-buckets --query \"Buckets[].Name\""

    # Deploy cluster autoscaler test
    #kubectl apply -f nginx-autoscaler-test.yaml

    # API Gateway test - Deploy Echoserver 
    #kubectl apply -f echoserver.yaml
    
    # Deploy API Gateway
    echo "INFO: Stage 3/3 - Creating API Gateway, Cognito"
    cd ../eks

    echo "INFO: Set TF vars for terraform run"
    #export TF_VAR_echo_nlb_listenter_arn=$(./get_lb_listener_arn.sh echoserver staging svc)
    #export TF_VAR_game2048_alb_listenter_arn=$(./get_lb_listener_arn.sh ingress-2048 game-2048 ingress)
    #export TF_VAR_goshares_nlb_listenter_arn=$(./get_lb_listener_arn.sh goshares-lb goshares svc)
    #export TF_VAR_goshares_alb_listenter_arn=$(./get_lb_listener_arn.sh anycompany-ingress goshares ingress)
    export TF_VAR_goshares_alb_listenter_arn=$(./get_lb_listener_arn.sh anymarket-ingress anymarket ingress)

    terraform init
    terraform apply --auto-approve

fi