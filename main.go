package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"strings"
	"time"
	"github.com/aws/aws-sdk-go/aws"
    "github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/cloudwatch"
)

// NetDebug ... turn on or off Debug mode for the Net Backend
var NetDebug bool

// Debug ... turn on or off Debug mode
var Debug bool

var delta52WeeksMin, delta52WeeksThrHigh, delta52WeeksThrLow float64
var buyfile, buyfileSet = os.LookupEnv("BUYFILE")
var DataPrefix, DataPrefixSet = os.LookupEnv("DATAPREFIX")
var refreshInterval int
var getCmd, serverCmd, daemonCmd bool

func processBuyRecommendations(allGroups []*Group, screenerGroup *ScreenerGroup) {
	for _, group := range allGroups {
		fmt.Println(fmt.Sprintf("INFO: Processing Buy Recommendations for %s", group.Name))

		// Keep track of the group average
		var week52highAverageDeltaSum float64 = 0
		n := 0

		for _, category := range group.Categories {
			br := category.getBuyRecommendations(screenerGroup)
			fmt.Println(category.renderCategoryBuyRecommendationsStr(br))
			if category.NumOfSecurities > 0 {
				week52highAverageDeltaSum = week52highAverageDeltaSum + br.Week52HighAvg
				n++
			}
		}

		week52highAverageDelta := week52highAverageDeltaSum / float64(n)

		color := Colors.Green
		if week52highAverageDelta <= 50 {
			if week52highAverageDelta > 25 {
				color = Colors.Orange
			} else {
				color = Colors.Red
			}
		}

		currentDateTime := time.Now()
		date := currentDateTime.Format(time.ANSIC)
		fmt.Println(fmt.Sprintf("%s: %sGroup %s Avg Strength (price avg of 52 weeks high): %s%.2f%%%s\n", date, Colors.Bold, group.Name, color, week52highAverageDelta, Colors.Reset))

		//group.displayGroupStrength()
	}
}

// Parse the strategy file where all Groups are defined
func parseStrategyFile(readBackend Backend) []*Group {

	var categoryFile, set = os.LookupEnv("CATEGORY_FILE")
	if !set {
		categoryFile = DataPrefix + "strategy.json"
	}

	// Open our jsonFile
	jsonFile, err := os.Open(categoryFile)
	if err != nil {
		fmt.Println(err)
	}
	defer jsonFile.Close()

	//Declare a map variable
	var strategyJSON map[string]map[string]string

	// Read file as byte array
	byteValue, _ := ioutil.ReadAll(jsonFile)
	json.Unmarshal([]byte(byteValue), &strategyJSON)

	allGroups := makeGroups(strategyJSON, &readBackend)

	return allGroups
}

func init() {
	// Lookup DEBUG environment variable and set it globally
	v := os.Getenv("DEBUG")
	var err error
	Debug, err = strconv.ParseBool(v)
	if err != nil {
		Debug = false
	}

	// Lookup NET_DEBUG environment variable and set it globally
	v = os.Getenv("NET_DEBUG")
	NetDebug, err = strconv.ParseBool(v)
	if err != nil {
		NetDebug = false
	}

	// Set flags
	flag.IntVar(&refreshInterval, "n", -1, "Specifies the number of seconds in which to display buy recommendations again.")
	flag.BoolVar(&getCmd, "get", false, "Downloads the latest stats for all securities using API")
	flag.BoolVar(&daemonCmd, "daemon", false, "Gets the price for each company and stores in a datastore backend")
	flag.BoolVar(&serverCmd, "server", false, "Start the goshares server on port 32000")

	// Check Environment variables
	d := os.Getenv("DELTA52MIN")
	if delta52WeeksMin, err = strconv.ParseFloat(d, 64); err != nil {
		delta52WeeksMin = 5
	}
	log.Println(fmt.Sprintf("INFO: DELTA52MIN - Using minimum 52 Week high delta: %.2f%%", delta52WeeksMin))

	x := os.Getenv("HIGH52THRESHOLD")
	if delta52WeeksThrHigh, err = strconv.ParseFloat(x, 64); err != nil {
		delta52WeeksThrHigh = 5
	}
	log.Println(fmt.Sprintf("INFO: HIGH52THRESHOLD - Using 52 Week Strength high Threshold: %.2f%%", delta52WeeksThrHigh))

	y := os.Getenv("LOW52THRESHOLD")
	if delta52WeeksThrLow, err = strconv.ParseFloat(y, 64); err != nil {
		delta52WeeksThrLow = 5
	}
	log.Println(fmt.Sprintf("INFO: LOW52THRESHOLD - Using 52 Week Strength Low Threshold: %.2f%%", delta52WeeksThrLow))

}

func loadAWSSession() (*session.Session,bool) {
	sess := session.Must(session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigEnable,
	}))

	// Create new cloudwatch client.
    svc := cloudwatch.New(sess)

	_, err := svc.PutMetricData(&cloudwatch.PutMetricDataInput{
        Namespace: aws.String("AnyMarket/application"),
        MetricData: []*cloudwatch.MetricDatum{
            &cloudwatch.MetricDatum{
                MetricName: aws.String("PodStart"),
                Unit:       aws.String("Count"),
                Value:      aws.Float64(1),
				Timestamp:	aws.Time(time.Now()),
                Dimensions: []*cloudwatch.Dimension{
                    &cloudwatch.Dimension{
                        Name:  aws.String("Compute"),
                        Value: aws.String("EKS"),
                    },
                },
            },
        },
    })
    if err != nil {
        log.Println("ERROR: Error adding metrics:", err.Error())
		return sess, true
		
    }else{
		log.Println("INFO: Added CloudWatch metric")
		return sess, false
	}

}

func main() {
	flag.Parse()

	if !DataPrefixSet {
		DataPrefix = "data/"
	} else {
		//Append / if it does not exist
		if !strings.HasSuffix(DataPrefix, "/") {
			DataPrefix = DataPrefix + "/"
		}
	}

	log.Println("INFO: Using Data directory:", DataPrefix)

	fileBackend := new(FileBackend)
	fileBackend.DataPrefix = DataPrefix
	netBackend := new(NetBackend)
	cacheBackend := new (CacheBackend)
	cacheBackend.init()

	var allgroups []*Group

	// Check if its a Daemon command, if so just get price and continue
	if daemonCmd {
		// Refresh latest stats
		allgroups = parseStrategyFile(netBackend)

		if refreshInterval > 0 {
			for true {
				//TODO: Implement writing shares to Redis cache
				// https://tutorialedge.net/golang/go-redis-tutorial/
				//for _, g := range allgroups {
				//g.saveGroupShares(fileBackend)
				//}
				log.Println(fmt.Sprintf("INFO: Next price refresh in %d seconds. Ctrl-C to exit", refreshInterval))
				time.Sleep(time.Duration(refreshInterval) * time.Second)
			}
		}
	}

	// Load pre-defined groups and categories in strategy.json
	if getCmd {
		// Refresh latest stats
		allgroups = parseStrategyFile(netBackend)
		for _, g := range allgroups {
			// Save stats
			g.saveGroupShares(fileBackend)
		}
	} else {
		// Load previously saved stats
		allgroups = parseStrategyFile(fileBackend)
	}

	// Load buy recommendation json
	// TODO: check for error
	sg, _ := fileBackend.LoadScreenerGroup("default")
	screeners := sg.Screeners
	for _, sc := range screeners {
		log.Println("INFO: Loaded screener:", sc.Name)
	}

	//Start price cache thread for every 15 mins
	//go initPriceCacheRefresher(3600)

	if serverCmd {
		var APIGroup *Group
		for _, g := range allgroups {
			if g.Name == "S&P500" {
				APIGroup = g
				log.Println("INFO: Using group for server:", APIGroup.Name)
				break
			}
		}

		log.Println("INFO: Server initializing")
		HTTPServer = &Server{}
		HTTPServer.Name = "default"
		HTTPServer.ReadBackend = netBackend
		HTTPServer.ScreenerBackend = fileBackend
		HTTPServer.APIGroup = APIGroup


		// Lookup ENABLEAWSCLOUDWATH environment variable and set it globally
		var enableCloudWatch bool
		v := os.Getenv("ENABLEAWSCLOUDWATH")
		enableCloudWatch, _ = strconv.ParseBool(v)

		if enableCloudWatch {
			log.Println("INFO: Initializing AWS SDK")
			session, is_error := loadAWSSession()
	
			if is_error  {
				log.Println("WARN: Not using AWS Session for metrics - check earlier errors")
			}else{
				HTTPServer.AWSSession = session
				HTTPServer.AWSCloudWatch = true
			}
		}else{
			log.Println("INFO: Not using AWS Cloud Watch Metrics. Set ENABLEAWSCLOUDWATH=True if needed.")
		}

		log.Println("INFO: Server initialized")
		handleRequests()

	} else { //Continue in command line mode
		processBuyRecommendations(allgroups, sg)

		if refreshInterval > 0 {
			for true {
				log.Println(fmt.Sprintf("INFO: Next refresh in %d seconds. Ctrl-C to exit", refreshInterval))
				time.Sleep(time.Duration(refreshInterval) * time.Second)
				processBuyRecommendations(allgroups, sg)
			}
		}
	}

}
