// iex.go implements the data interface to download data from a Financial API service
package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
)

var apiKey = os.Getenv("IEX_KEY")
var baseURL = "https://cloud.iexapis.com/stable"
var companyEndpoint = "/stock/{}/company"
var priceEndpoint = "/stock/%s/price"
var statsEndpoint = "/stock/%s/stats"
var analystEndpoint = "/stock/%s/price-target"
var rsiEndpoint = "/stock/%s/indicator/rsi?range=2m&input1=%d&lastIndicator=true&indicatorOnly=true"
var smaEndpoint = "/stock/%s/indicator/sma?range=12m&input1=%d&lastIndicator=true&indicatorOnly=true"
var token = fmt.Sprintf("?token=%s", apiKey)
var andToken = fmt.Sprintf("&token=%s", apiKey)

var PriceCache = make(map[string]float64)

// Technical indicator struct
type indicator struct {
	Indicators [][]float64 `json:"indicator"`
	Chart      []string    `json:"chart"`
}

// Price Analyst struct
type analyst struct {
	Symbol             string  `json:"symbol"`
	UpdatedDate        string  `json:"updatedDate"`
	PriceTargetAverage float64 `json:"priceTargetAverage"`
	PriceTargetHigh    float64 `json:"priceTargetHigh"`
	PriceTargetLow     float64 `json:"priceTargetLow"`
	NumberOfAnalysts   float64 `json:"numberOfAnalysts"`
	Currency           string  `json:"currency"`
}

// Reads the specified URL by prepending the base URL and appending the token
// Returns result as a String
// TODO: Add retry logic
func readURL(requestURL string) string {

	if NetDebug {
		fmt.Println(fmt.Sprintf("DEBUG: Request URL:  -> %s%s", baseURL, requestURL))
	}

	// This is the request URL with the API token appended
	var readURL string

	if len(apiKey) > 0 {
		//Append the token wither either ? or &
		if strings.Contains(requestURL, "?") {
			readURL = fmt.Sprintf("%s%s%s", baseURL, requestURL, andToken)
		} else {
			readURL = fmt.Sprintf("%s%s%s", baseURL, requestURL, token)
		}
	} else {
		log.Println("ERROR: Please set the IEX_KEY Environment variable")
		return ""
	}

	response, err := http.Get(readURL)

	if err != nil {
		log.Println("ERROR: Received error from HTTP GET request", err)
	}
	defer response.Body.Close()

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Println("ERROR: Received error from response", err)
	}

	resultStr := fmt.Sprintf(string(body))
	if NetDebug {
		fmt.Println(fmt.Sprintf("DEBUG: Response:  -> %s", resultStr))
	}

	return resultStr
}

// Gets the SMA of specified period
func getSMA(symbol string, period int) float64 {
	var result float64

	var url = fmt.Sprintf(smaEndpoint, symbol, period)
	var response = readURL(url)
	var indicator indicator

	json.Unmarshal([]byte(response), &indicator)
	if NetDebug {
		fmt.Println(fmt.Sprintf("DEBUG: indicator struct -> %v", indicator))
	}

	if len(indicator.Indicators) > 0 && len(indicator.Indicators[0]) > 0 {
		result = indicator.Indicators[0][0]
	}

	return result
}

// Gets the RSI of specified period
func getRSI(symbol string, period int) float64 {
	var result float64

	var url = fmt.Sprintf(rsiEndpoint, symbol, period)
	var response = readURL(url)
	var indicator indicator

	json.Unmarshal([]byte(response), &indicator)
	if NetDebug {
		fmt.Println(fmt.Sprintf("DEBUG: indicator struct -> %v", indicator))
	}

	if len(indicator.Indicators) > 0 && len(indicator.Indicators[0]) > 0 {
		result = indicator.Indicators[0][0]
	}

	return result
}

// Get Analyst recommendations
func getAnalystTargets(symbol string) analyst {
	var result analyst

	var url = fmt.Sprintf(analystEndpoint, symbol)
	var response = readURL(url)
	var analyst analyst

	json.Unmarshal([]byte(response), &analyst)
	if NetDebug {
		fmt.Println(fmt.Sprintf("DEBUG: analyst struct -> %v", analyst))
	}

	if len(analyst.Symbol) > 0 {
		result = analyst
	}

	return result
}

// Gets the latest price for a security
func getPrice(symbol string) float64 {
	var result float64

	//TODO: Use concurrency safe PriceCache
	// Currently due to high concurrency, PriceCache can only be updated during startup
	if pch, ok := PriceCache[symbol]; ok {
		//Return result from cache
		result = pch

		if NetDebug {
			fmt.Println("DEBUG: Cache Hit!  ---->", symbol, pch)
		}

	} else {
		var url = fmt.Sprintf(priceEndpoint, symbol)

		priceStr := readURL(url)

		price, err := strconv.ParseFloat(priceStr, 64)

		result = price

		if err != nil {
			log.Println(fmt.Sprintf("ERROR: Could not convert price string for %s, to double value: %s", symbol, priceStr), err)
		} else {
			PriceCache[symbol] = price
		}
	}

	return result
}

// Get stats associated for this security
func getStats(symbol string) map[string]interface{} {
	var url = fmt.Sprintf(statsEndpoint, symbol)

	statsStr := readURL(url)

	var statsMap map[string]interface{} = make(map[string]interface{})
	json.Unmarshal([]byte(statsStr), &statsMap)
	if NetDebug {
		fmt.Println(fmt.Sprintf("DEBUG: Stats Map -> %v", statsMap))
	}

	return statsMap
}

/* func initPriceCacheRefresher(secs time.Duration){
	fmt.Println("INFO: Cache refresher initiated")
	for {
		time.Sleep(secs * time.Second)
		n := len(PriceCache)
		for k := range PriceCache {
			delete(PriceCache, k)
		}
		fmt.Println("INFO: Cleared PriceCache with length: ", n)
    }
} */
