package main

import (
	"fmt"
	"log"
	"sync"
)

// Group represents a set of categories
type Group struct {
	Name       string
	Categories map[string]*Category
}

func (g *Group) saveGroupShares(writeBackend Backend) {
	var wg sync.WaitGroup
	for _, category := range g.Categories {
		wg.Add(1)
		go category.saveCategoryShares(writeBackend, &wg)
		wg.Wait()
	}

}

func makeGroups(groupsMap map[string]map[string]string, readBackend *Backend) []*Group {
	//Initialize an array to hold all the groups
	var allGroups []*Group
	allGroups = make([]*Group, 0)

	// Parse json
	for grpName, value := range groupsMap {
		// Each value is an interface{} type, that is type asserted as a string
		log.Println(fmt.Sprintf("INFO: Loading Group: %s", grpName))
		g := Group{}
		g.Name = grpName
		g.Categories = make(map[string]*Category)

		allGroups = append(allGroups, &g)

		// Parse the categories
		var categories map[string]string = value

		for catName, securitiesStr := range categories {
			c := makeCategoryFromString(catName, securitiesStr, readBackend)
			g.Categories[catName] = &c
		}

	}

	return allGroups
}
