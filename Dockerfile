#FROM alpine:3.14
FROM ubuntu:20.04

# author
MAINTAINER Kawsar

COPY ./goshares /goshares
RUN chmod +x /goshares

RUN apt-get update -y; apt-get install curl -y

EXPOSE 32000

ADD ./data /data
CMD ["/goshares","-server"]
