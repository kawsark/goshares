module gitlab.com/kawsark/goshares

go 1.16

require (
	github.com/aws/aws-sdk-go v1.44.142
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/golang-jwt/jwt/v4 v4.4.2
	github.com/julienroland/usg v0.0.0-20160918114137-cb52eabb3d84
	github.com/stretchr/testify v1.7.0 // indirect
)
