#!/bin/bash

[[ -z $1 ]] && echo "Usage: get_lb_listener_arn.sh <svc> <ns> <ingress|svc>" && exit 1
[[ -z $2 ]] && echo "Usage: get_lb_listener_arn.sh <svc> <ns> <ingress|svc>" && exit 1
[[ -z $3 ]] && echo "Usage: get_lb_listener_arn.sh <svc> <ns> <ingress|svc>" && exit 1

# usage: get_lb_listener_arn.sh <svc> <ns> <svc/ingress>
export lb_external_ip=$(kubectl get $3/$1 -n $2 | grep $1 | awk '{print $4}')

aws elbv2 describe-load-balancers --query LoadBalancers[*] > elb.json
lb_arn=$(jq -r --arg DNS "$lb_external_ip" '.[] | select(.DNSName==$DNS) | .LoadBalancerArn' elb.json)
lis_arn=$(aws elbv2 describe-listeners --load-balancer-arn "${lb_arn}" | jq -r .Listeners[0].ListenerArn)
echo ${lis_arn}