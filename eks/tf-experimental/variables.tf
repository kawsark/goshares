variable "aws_region" {
  description = "Region where EKS should be provisioned"
  default     = "us-east-2"
}

variable "cluster_name" {
  description = "name of the EKS cluster"
  default     = "eks-demo"
}

variable "cluster_version" {
  default = "1.22"
}

variable "owner_tag" {
  default = "demouser"
}

variable "eks_node_group_instance_types" {
  default = "m5.large"
}
variable "eks_public_access_cidrs" {
  # https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/eks_cluster
  description = "Ingress CIDR to allow EKS API access. Warning: setting 0.0.0.0/0 is a bad idea."
  type        = list(string)
  default     = ["1.1.1.1/32"]
}