resource "aws_eip" "nat" {
  vpc = true

  tags = {
    Name  = "nat"
    owner = var.owner_tag
  }
}

resource "aws_nat_gateway" "nat" {
  allocation_id = aws_eip.nat.id
  subnet_id     = aws_subnet.public-subnet-a.id

  tags = {
    Name  = "nat"
    owner = var.owner_tag
  }

  depends_on = [aws_internet_gateway.igw]
}
