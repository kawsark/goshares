# Experimental
- There is an issue with this Terraform module which causes Node joins to be unsuccessful.
- For goshares, we will pivot to using `eksctl` for the time being
- References:
  - https://antonputra.com/terraform/how-to-create-eks-cluster-using-terraform/
  - https://antonputra.com/amazon/create-aws-eks-fargate-using-terraform/
