#!/bin/bash

export PATH=$(pwd):${PATH}
[[ -z $1 ]] && echo "Usage: ab_benchmark.sh <cognitouser>" && exit 1
user=$1

kubectl scale deployment anymarket --replicas=2 -n anymarket
sleep 60
ab_test.sh ${user} 1 100
ab_test.sh ${user} 5 500
ab_test.sh ${user} 10 1000
kubectl scale deployment anymarket --replicas=4 -n anymarket
sleep 120
ab_test.sh ${user} 100 10000
ab_test.sh ${user} 200 20000
ab_test.sh ${user} 300 30000
ab_test.sh ${user} 400 40000
kubectl scale deployment anymarket --replicas=6 -n anymarket
sleep 120
ab_test.sh ${user} 500 50000
ab_test.sh ${user} 600 60000
ab_test.sh ${user} 700 70000
ab_test.sh ${user} 800 80000
kubectl scale deployment anymarket --replicas=8 -n anymarket
sleep 120
ab_test.sh ${user} 900 90000
ab_test.sh ${user} 1000 100000

target=2
kubectl scale deployment anymarket --replicas=$target -n anymarket
