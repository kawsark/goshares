output "cognito_pool_client_id" { 
  value = aws_cognito_user_pool_client.client.id
}

output "user_pool_id" { 
  value = aws_cognito_user_pool.pool.id
}

# API Gateway endpoints
output "app_dev_base_url" {
  value = "${aws_apigatewayv2_stage.dev.invoke_url}"
}

output "app_default_base_url" {
  value = "${aws_apigatewayv2_stage.default.invoke_url}"
}

output "dev_stage_base_url" {
  value = aws_apigatewayv2_stage.dev.invoke_url
}

output "default_stage_base_url" {
  value = aws_apigatewayv2_stage.default.invoke_url
}

output "main_api_endpoint" {
  value = aws_apigatewayv2_api.main.api_endpoint
}

output "main_api_id" {
  value = aws_apigatewayv2_api.main.id
}