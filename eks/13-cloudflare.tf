# Add the server record to domain
resource "cloudflare_record" "tradebotdns_server" {
  zone_id = var.cloudflare_zone_id
  name   = var.domain_name_label
  value  = "${aws_apigatewayv2_api.main.id}.execute-api.us-east-2.amazonaws.com"
  type   = "CNAME"
  ttl    = 1
}