#!/bin/bash

usage="Usage: ab_test.sh <username> <concurrency> <requests>"
if [[ -z $1 ]] || [[ -z $2 ]] || [[ -z $3 ]]; then
  echo $usage
  exit 1
fi

# Get these from terraform output
export url=$(terraform output --raw app_default_base_url)
export CLIENT_ID=$(terraform output --raw cognito_pool_client_id)
export USER_POOL_ID=$(terraform output --raw user_pool_id)

export USER_NAME=$1
export PASSWORD=$(cat cognitotest.${1})

aws cognito-idp initiate-auth \
 --client-id ${CLIENT_ID} \
 --auth-flow USER_PASSWORD_AUTH \
 --auth-parameters USERNAME=${USER_NAME},PASSWORD=${PASSWORD} \
 --query 'AuthenticationResult.IdToken' \
 --output text > jwt.txt

export TOKEN=$(cat jwt.txt)

n=$(kubectl get pods -n anymarket | grep Running | wc | awk '{print $1}')

f=/tmp/ab-$2-$3-$n.txt
echo "INFO: Output file is: ${f}"

# add "-v 2" for more verbose
ab -p ../payload3.json -T application/json -H "Authorization: ${TOKEN}" -c $2 -n $3 ${url} > ${f} 2> /dev/null

#echo "INFO: Test completed, 200 Count: $(cat ${f} | grep "200 OK" | wc)"

echo "# of pods in anymarket ns: $n" | tee -a ${f}

tail -n 35 ${f}

# Backup to S3 and delete file
aws s3 cp ${f} s3://anymarket-benchmarks
rm ${f}