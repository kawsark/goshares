#!/bin/bash

source env.sh

# Deploy the Metrics server
# https://docs.aws.amazon.com/eks/latest/userguide/metrics-server.html
echo "INFO: 1. Deploying Metrics server"
kubectl apply -f metrics-server.yaml
kubectl get deployment metrics-server -n kube-system

# Deploy cluster Auto-scaler
# https://github.com/kubernetes/autoscaler/blob/master/cluster-autoscaler/cloudprovider/aws/README.md
echo "INFO: 2. Deploying Cluster auto-scaler"
sed -i -e 's/ACCOUNTID/'"${ACCOUNT_ID}"'/g' -e 's/CLUSTERNAME/'"${cluster_name}"'/g' cluster-autoscaler-autodiscover.yaml
kubectl apply -f cluster-autoscaler-autodiscover.yaml
git restore -- cluster-autoscaler-autodiscover.yaml

# Create AWS Load balancer controller
# https://docs.aws.amazon.com/eks/latest/userguide/aws-load-balancer-controller.html
echo "INFO: 3. Installing the Load Balancer Controller"
curl -o iam_policy.json https://raw.githubusercontent.com/kubernetes-sigs/aws-load-balancer-controller/v2.4.4/docs/install/iam_policy.json

aws iam create-policy \
    --policy-name AWSLoadBalancerControllerIAMPolicy \
    --policy-document file://iam_policy.json

eksctl create iamserviceaccount \
    --cluster=${cluster_name} \
    --namespace=kube-system \
    --name=aws-load-balancer-controller \
    --role-name "AmazonEKSLoadBalancerControllerRole" \
    --attach-policy-arn=arn:aws:iam::${ACCOUNT_ID}:policy/AWSLoadBalancerControllerIAMPolicy \
    --approve

helm repo add eks https://aws.github.io/eks-charts
helm repo update
helm install aws-load-balancer-controller eks/aws-load-balancer-controller \
    -n kube-system \
    --set clusterName=${cluster_name} \
    --set serviceAccount.create=false \
    --set image.repository=602401143452.dkr.ecr.${aws_region}.amazonaws.com/amazon/aws-load-balancer-controller \
    --set serviceAccount.name=aws-load-balancer-controller

kubectl get deployment -n kube-system aws-load-balancer-controller

# Install the EFS CSI Driver
# https://docs.aws.amazon.com/eks/latest/userguide/efs-csi.html
echo "INFO: 4. Installing the EFS CSI Driver"

curl -o iam-policy-example.json https://raw.githubusercontent.com/kubernetes-sigs/aws-efs-csi-driver/master/docs/iam-policy-example.json
aws iam create-policy \
    --policy-name AmazonEKS_EFS_CSI_Driver_Policy \
    --policy-document file://iam-policy-example.json

eksctl create iamserviceaccount \
    --cluster ${cluster_name} \
    --namespace kube-system \
    --name efs-csi-controller-sa \
    --attach-policy-arn arn:aws:iam::${ACCOUNT_ID}:policy/AmazonEKS_EFS_CSI_Driver_Policy \
    --approve \
    --region ${aws_region}

helm repo add aws-efs-csi-driver https://kubernetes-sigs.github.io/aws-efs-csi-driver/

helm repo update

helm upgrade -i aws-efs-csi-driver aws-efs-csi-driver/aws-efs-csi-driver \
    --namespace kube-system \
    --set image.repository=602401143452.dkr.ecr.${aws_region}.amazonaws.com/eks/aws-efs-csi-driver \
    --set controller.serviceAccount.create=false \
    --set controller.serviceAccount.name=efs-csi-controller-sa

echo "INFO: 5. Describing cluster OIDC server URL"
aws eks describe-cluster --name $cluster_name --query "cluster.identity.oidc.issuer"

