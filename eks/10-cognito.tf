resource "aws_cognito_user_pool" "pool" {
  name = "GosharesPool"

  email_verification_subject = "Your Verification Code"
  alias_attributes           = ["email"]
  auto_verified_attributes   = ["email"]

  password_policy {
    minimum_length    = 10
    temporary_password_validity_days = 7
    require_lowercase = true
    require_numbers   = true
    require_symbols   = false
    require_uppercase = false
  }

  username_configuration {
    case_sensitive = false
  }

  schema {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "email"
    required                 = true

    string_attribute_constraints {
      min_length = 7
      max_length = 256
    }
  }

  schema {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "name"
    required                 = true

    string_attribute_constraints {
      min_length = 3
      max_length = 256
    }
  }

  schema {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "company"
    required                 = false

    string_attribute_constraints {
      min_length = 3
      max_length = 256
    }
  }
}

resource "aws_cognito_user_pool_client" "client" {
  name                         = "GosharesPoolClient"
  user_pool_id = aws_cognito_user_pool.pool.id


  explicit_auth_flows          = ["ALLOW_USER_PASSWORD_AUTH", 
                                  "ALLOW_REFRESH_TOKEN_AUTH",
                                  "ALLOW_USER_SRP_AUTH"]

  allowed_oauth_flows_user_pool_client = true

  allowed_oauth_flows = [ "implicit" ]

  allowed_oauth_scopes = [ "aws.cognito.signin.user.admin", "email", "openid", "phone", "profile" ]

  supported_identity_providers = [ "COGNITO" ]

  id_token_validity = "12"

  access_token_validity = "12"


}

resource "aws_cognito_user_pool_domain" "main" {
  domain       = "goshares-auth"
  user_pool_id = aws_cognito_user_pool.pool.id
}