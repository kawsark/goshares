#!/bin/bash

export CLIENT_ID=$(terraform output --raw cognito_pool_client_id)
export USER_POOL_ID=$(terraform output --raw user_pool_id)

if [[ ! -z $1 ]]; then
  export USER_NAME=$1
  export PASSWORD=$(cat cognitotest.${1})
else
  echo "INFO: New user sign-up"

  export USER_NAME=$(date | md5sum | cut -c 1-10)
  export PASSWORD=$(date | md5sum | awk '{print $1}')
  EMAIL=${USER_NAME}@customer1.com
  touch cognitotest.${USER_NAME}

  echo ${PASSWORD} > cognitotest.${USER_NAME}

  aws cognito-idp sign-up \
    --client-id ${CLIENT_ID} \
    --username ${USER_NAME} \
    --password ${PASSWORD} \
    --user-attributes Name=name,Value=${NAME} Name=email,Value=${EMAIL}

  aws cognito-idp admin-confirm-sign-up  \
    --user-pool-id ${USER_POOL_ID} \
    --username ${USER_NAME}
fi

#aws cognito-idp admin-get-user \
#  --user-pool-id ${USER_POOL_ID} \
#  --username ${USER_NAME} --output text


aws cognito-idp initiate-auth \
 --client-id ${CLIENT_ID} \
 --auth-flow USER_PASSWORD_AUTH \
 --auth-parameters USERNAME=${USER_NAME},PASSWORD=${PASSWORD} \
 --query 'AuthenticationResult.IdToken' \
 --output text | tee jwt.txt

export TOKEN=$(cat jwt.txt)


#url=$(terraform output --raw app_dev_base_url)
url=$(terraform output --raw app_default_base_url)
dev_url="$(terraform output --raw app_dev_base_url)/"

echo "INFO: url is ${url}"

curl -X POST --header "Authorization: ${TOKEN}" -d "@../payload2.json" ${url}

#ab -p ../payload2.json -T application/json -H "Authorization: ${TOKEN}" -c 1 -n 1 ${url}

#echo "aws cognito-idp admin-delete-user --user-pool-id $USER_POOL_ID --username ${USER_NAME}" >> cognito_cleanup.txt

