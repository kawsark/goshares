resource "aws_vpc_endpoint" "ecr_dkr" {
  vpc_id          = var.vpc_id
  service_name    = "com.amazonaws.${var.aws_region}.ecr.dkr"
  vpc_endpoint_type = "Interface"

  subnet_ids = var.private_subnet_ids
  #TODO: Use a Data source for this
  security_group_ids = var.nodegroup_security_group_ids

  tags = {
    Name = "ecr-docker-endpoint"
  }
}

resource "aws_vpc_endpoint" "ecr_api" {
  vpc_id          = var.vpc_id
  service_name    = "com.amazonaws.${var.aws_region}.ecr.api"
  vpc_endpoint_type = "Interface"

  subnet_ids = var.private_subnet_ids
  #TODO: Use a Data source for this
  security_group_ids = var.nodegroup_security_group_ids

  tags = {
    Name = "ecr-api-endpoint"
  }
}

resource "aws_vpc_endpoint" "dynamodb" {
  vpc_id          = var.vpc_id
  service_name    = "com.amazonaws.${var.aws_region}.dynamodb"
  route_table_ids = var.private_route_table_ids

  tags = {
    Name = "dynamodb-endpoint"
  }
}

resource "aws_vpc_endpoint" "elasticfilesystem" {
  vpc_id          = var.vpc_id
  service_name    = "com.amazonaws.${var.aws_region}.elasticfilesystem"
  vpc_endpoint_type = "Interface"

  subnet_ids = var.private_subnet_ids
  #TODO: Use a Data source for this
  security_group_ids = var.nodegroup_security_group_ids

  tags = {
    Name = "elasticfilesystem-endpoint"
  }
}