#!/bin/bash

echo "INFO: Please run source env.sh before execuing this script"

[[ -z $cluster_name ]] && echo "ERROR: Please set cluster_name" && exit 1
[[ -z $ACCOUNT_ID ]] && echo "ERROR: Please set ACCOUNT_ID" && exit 1
[[ -z $aws_region ]] && echo "ERROR: Please set aws_region" && exit 1

cluster_check=$(eksctl get clusters --name ${cluster_name})

if [[ -z ${cluster_check} ]]; then
    echo "INFO: Did not find cluster"

    if [[ $1 == "--destroy" ]]; then
        echo "INFO: NOOP"
    else
        echo "INFO: Creating new cluster"
        eksctl create cluster -f cluster-existing-network.yaml

        echo "INFO: Installing add-ons"
        ./eksaddons.sh

    fi
    
else
    echo "INFO: Found cluster - ${cluster_check}"

    if [[ $1 == "--destroy" ]]; then
        echo "INFO: Destroying cluster"
        eksctl delete cluster -f cluster-existing-network.yaml
    fi

fi
