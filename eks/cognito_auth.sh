#!/bin/bash

[[ ! -z $TOKEN ]] && echo "TOKEN is already set, reuse or unset TOKEN first" && exit 0

export CLIENT_ID=$(terraform output --raw cognito_pool_client_id)
export USER_POOL_ID=$(terraform output --raw user_pool_id)

USER_NAME=$(date | md5sum | cut -c 1-10)
PASSWORD=$(date | md5sum | awk '{print $1}')
EMAIL=${USER_NAME}@customer1.com
touch cognitotest.${USER_NAME}

echo ${PASSWORD} > cognitotest.${USER_NAME}

aws cognito-idp sign-up \
 --client-id ${CLIENT_ID} \
 --username ${USER_NAME} \
 --password ${PASSWORD} \
 --user-attributes Name=name,Value=${NAME} Name=email,Value=${EMAIL}

aws cognito-idp admin-confirm-sign-up  \
  --user-pool-id ${USER_POOL_ID} \
  --username ${USER_NAME}

#aws cognito-idp admin-get-user \
#  --user-pool-id ${USER_POOL_ID} \
#  --username ${USER_NAME}

aws cognito-idp initiate-auth \
 --client-id ${CLIENT_ID} \
 --auth-flow USER_PASSWORD_AUTH \
 --auth-parameters USERNAME=${USER_NAME},PASSWORD=${PASSWORD} \
 --query 'AuthenticationResult.IdToken' \
 --output text | tee jwt.txt

url=$(terraform output --raw app_dev_base_url)
export TOKEN=$(cat jwt.txt)

curl -X POST --header "Authorization: ${TOKEN}" -d "@../payload1.json" ${url}/

#ab -p ../payload.json -T application/json -H "Authorization: ${TOKEN}" -c 10 -n 2000 ${url}/

echo "aws cognito-idp admin-delete-user --user-pool-id $USER_POOL_ID --username ${USER_NAME}" >> cognito_cleanup.txt

