variable "cloudflare_zone_id" {
	 default = "anymarket.cloud"
}

variable "cloudflare_domain" {
	 default = "anymarket.cloud"
}

variable "domain_name_label" {
	 default = "@"
}

variable "cluster_name" {
  
}

variable "vpc_id" {
  
}

variable "private_subnet_ids" {
  
}

variable "private_route_table_ids" {
  
}

variable "nodegroup_security_group_ids" {
  
}

variable "aws_region" {
  default = "us-east-2"
}

variable "goshares_nlb_listenter_arn" {
  description = "The arn of the NLB listenter that will be associated with this API Gateway"
}

variable "goshares_alb_listenter_arn" {
  description = "The arn of an ALB listenter that will be associated with this API Gateway"
}