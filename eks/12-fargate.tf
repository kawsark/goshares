# https://docs.aws.amazon.com/eks/latest/userguide/pod-execution-role.html
# You must specify a pod execution role for the Amazon EKS components that run on the Fargate infrastructure using the profile. 
# This role is added to the cluster's Kubernetes Role based access control (RBAC) for authorization. 
# This allows the kubelet that's running on the Fargate infrastructure to register with your Amazon EKS cluster so that it can appear in your cluster as a node.
# The containers running in the Fargate pod can't assume the IAM permissions associated with a pod execution role. Use IRSA for that.

resource "aws_iam_role" "eks-fargate-profile" {
  name = "eks-fargate-profile"

  assume_role_policy = jsonencode({
    Statement = [{
      Action = "sts:AssumeRole"
      Effect = "Allow"
      Principal = {
        Service = "eks-fargate-pods.amazonaws.com"
      }
    }]
    Version = "2012-10-17"
  })
}

resource "aws_iam_role_policy_attachment" "eks-fargate-profile" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSFargatePodExecutionRolePolicy"
  role       = aws_iam_role.eks-fargate-profile.name
}

# Fargate profile selector for the anymarket namespace
resource "aws_eks_fargate_profile" "anymarket" {
  cluster_name           = var.cluster_name
  fargate_profile_name   = "anymarket"
  pod_execution_role_arn = aws_iam_role.eks-fargate-profile.arn

  # These subnets must have the following resource tag: 
  # kubernetes.io/cluster/<CLUSTER_NAME>.
  subnet_ids = var.private_subnet_ids

  selector {
    namespace = "anymarket"
  }
}

# Fargate profile selector for the Dev namespace
resource "aws_eks_fargate_profile" "dev" {
  cluster_name           = var.cluster_name
  fargate_profile_name   = "dev"
  pod_execution_role_arn = aws_iam_role.eks-fargate-profile.arn

  # These subnets must have the following resource tag: 
  # kubernetes.io/cluster/<CLUSTER_NAME>.
  subnet_ids = var.private_subnet_ids

  selector {
    namespace = "dev"
  }
}

# Fargate profile selector for the game-2048 namespace
resource "aws_eks_fargate_profile" "game-2048" {
  cluster_name           = var.cluster_name
  fargate_profile_name   = "game-2048"
  pod_execution_role_arn = aws_iam_role.eks-fargate-profile.arn

  # These subnets must have the following resource tag: 
  # kubernetes.io/cluster/<CLUSTER_NAME>.
  subnet_ids = var.private_subnet_ids

  selector {
    namespace = "game-2048"
  }
}

# Fargate profile selector for the Dev namespace
resource "aws_eks_fargate_profile" "staging" {
  cluster_name           = var.cluster_name
  fargate_profile_name   = "staging"
  pod_execution_role_arn = aws_iam_role.eks-fargate-profile.arn

  # These subnets must have the following resource tag: 
  # kubernetes.io/cluster/<CLUSTER_NAME>.
  subnet_ids = var.private_subnet_ids

  selector {
    namespace = "staging"
  }
}