#!/bin/bash

source bash_colors.sh

# Show company data being hosted on EFS - cat a stats file from pod
echo -e "$BYellow Read an Item from shared Amazon Elastic File System (EFS) $Color_Off"
read s
echo -e "$Yellow Running command: kubectl exec anymarket-cli -n anymarket -- cat /efs/AAPL-stats.json $Color_Off"
kubectl exec aws-cli -n anymarket -- cat /efs/AAPL-stats.json

# Show Dynamodb getItem
read s
clear
echo ""
echo -e "$BYellow Read an Item from DynamoDB Table: Anymarket-Customer1 $Color_Off"
read s
echo -e "$Yellow Running command: kubectl exec anymarket-cli -n anymarket -- /aws/get.sh Anymarket-customer1 AMZN $Color_Off"
echo -e "$Green"
kubectl exec aws-cli -n anymarket -- /aws/get.sh Anymarket-customer1 AMZN
echo -e "$Color_Off"

# Show Dynamodb getItem Unauthorized
read s
clear
echo -e "$BYellow Read an Item from DynamoDB Table: Anymarket-Customer2 $Color_Off"
read s
echo -e "$Yellow Running command: kubectl exec anymarket-cli -n anymarket -- /aws/get.sh Anymarket-customer2 AMZN $Color_Off"
echo -e "$Red"
kubectl exec aws-cli -n anymarket -- /aws/get.sh Anymarket-customer2 AMZN
echo -e "$Color_Off"

# Scale up deployment and show how long it takes to 2X pods
read s
n=$(kubectl get pods -n anymarket | grep "anymarket-.*Running" | wc | awk '{print $1}')
clear
echo -e "$BYellow Scale the Anymarket deployment. Current instances: $n Target instances? $Color_Off"
read s
target=$s
echo -e "$BYellow Scaling deployment $Color_Off"
echo -e "$Yellow Running command: kubectl scale deployment anymarket --replicas=$target -n anymarket $Color_Off"
start=`date +%s`
kubectl scale deployment anymarket --replicas=$target -n anymarket

for (( ; ; ))
do
   n=$(kubectl get pods -n anymarket | grep "anymarket-.*Running" | wc | awk '{print $1}')
   if [[ $n == $target ]]; then
     break
   fi
   sleep 2
done

end=`date +%s`
runtime=$((end-start))

echo -e "$Green Scaled to $t pods in $runtime seconds $Color_Off"

echo -e "$BYellow Scale the Anymarket deployment again? Current instances: $n Target instances? $Color_Off"
read s
target=$s
echo -e "$BYellow Scaling deployment $Color_Off"
echo -e "$Yellow Running command: kubectl scale deployment anymarket --replicas=$target -n anymarket $Color_Off"
start=`date +%s`
kubectl scale deployment anymarket --replicas=$target -n anymarket

# Lets look at some CloudWatch
echo -e "$BYellow Let's look at some Amazon CloudWatch Dashboards now $Color_off"
